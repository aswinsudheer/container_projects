# from requests import Request, Session
# from requests.exceptions import ConnectionError, Timeout, TooManyRedirects
# import json
#
# url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/historical'
# parameters = {
#     'start': '1',
#     'limit': '5000',
#     'convert': 'USD',
#     'date': '2023-10-26'
# }
# headers = {
#     'Accepts': 'application/json',
#     'X-CMC_PRO_API_KEY': '91964ba9-b72f-42d1-8e9e-1ba19c2c77a7',
# }
#
# session = Session()
# session.headers.update(headers)
#
# try:
#     response = session.get(url, params=parameters)
#     data = json.loads(response.text)
#     print(data)
# except (ConnectionError, Timeout, TooManyRedirects) as e:
#     print(e)
#
# -----------------------------------------------------

import requests
import pandas as pd
import schedule
import time
import csv
# Define your API key
api_key = '91964ba9-b72f-42d1-8e9e-1ba19c2c77a7'

# Define the API endpoint URL
url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'

# Define query parameters (optional)
# parameters = {
#     'limit': 10,  # Number of cryptocurrencies to retrieve (you can change this)
#     'convert': 'USD',  # Currency conversion (you can change this)
# }

# Set the headers with your API key
headers = {
    'X-CMC_PRO_API_KEY': api_key,
}
response = requests.get(url, headers=headers)

if response.status_code == 200:
    data = response.json()
    # Process and print the data
    print(data)
else:
    print(f"Request failed with status code: {response.status_code}")

cryptocurrencies = data['data']

# Create a DataFrame for the data
df = pd.DataFrame(cryptocurrencies)

# Save the DataFrame to a CSV file
csv_file = '/Users/aswinsudheer/Desktop/cryptocurrency_data.csv'
df.to_csv(csv_file, index=False)


# try:
#     # Make the API request
#     response = requests.get(url, headers=headers)
#
#     # Check if the request was successful
#     if response.status_code == 200:
#         data = response.json()
#         # Process and print the data
#         print(data)
#     else:
#         print(f"Request failed with status code: {response.status_code}")
# except Exception as e:
#     print(f"An error occurred: {e}")
# # ----------------------------------------------------

# import requests
# import csv
#
# # Define your API key
# api_key = '91964ba9-b72f-42d1-8e9e-1ba19c2c77a7'
#
# # Define the API endpoint URL
# url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest'
#
# # Define query parameters (optional)
# parameters = {
#     'limit': 10,  # Number of cryptocurrencies to retrieve (you can change this)
#     'convert': 'USD',  # Currency conversion (you can change this)
# }
#
# # Set the headers with your API key
# headers = {
#     'X-CMC_PRO_API_KEY': api_key,
# }
#
# try:
#     # Make the API request
#     response = requests.get(url, params=parameters, headers=headers)
#
#     # Check if the request was successful
#     if response.status_code == 200:
#         data = response.json()
#
#         # Extract the list of cryptocurrencies
#         cryptocurrencies = data['data']
#
#         # Define the CSV file name
#         csv_file = 'cryptocurrency_data.csv'
#
#         # Save data to CSV file
#         with open(csv_file, 'w', newline='') as file:
#             writer = csv.writer(file)
#
#             # Write the header row
#             writer.writerow(['Name', 'Symbol', 'Price (USD)'])
#
#             # Write data for each cryptocurrency
#             for crypto in cryptocurrencies:
#                 name = crypto['name']
#                 symbol = crypto['symbol']
#                 price = crypto['quote']['USD']['price']
#                 writer.writerow([name, symbol, price])
#
#         print(f'Data saved to {csv_file}')
#     else:
#         print(f"Request failed with status code: {response.status_code}")
# except Exception as e:
#     print(f"An error occurred: {e}")
#
